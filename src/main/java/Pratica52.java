/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */

import utfpr.ct.dainf.if62c.pratica.Equacao2Grau;


public class Pratica52 {
    public static void main(String[] args) {
        
        try
        {
            Equacao2Grau <Integer> raiz = new Equacao2Grau<>(1,2,-3);
            System.out.println(raiz.getRaiz1());
            System.out.println(raiz.getRaiz2());
            
            Equacao2Grau <Integer> raiza = new Equacao2Grau<>(5,6,10);
            System.out.println(raiza.getRaiz1());
            System.out.println(raiza.getRaiz2());
            
            Equacao2Grau <Integer> raizb = new Equacao2Grau<>(9,-12,4);
            System.out.println(raizb.getRaiz1());
            System.out.println(raizb.getRaiz2());
            
            raiz.setA(0);
        }
        catch (RuntimeException a){
            System.out.println(a.getLocalizedMessage());
        }
    }
}
